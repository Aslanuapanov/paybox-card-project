import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
export default new Vuex.Store({
  state: {
    cardNumber: '',
    cardName: '',
    cardMounth: '',
    cardYear: '',
    cardCCV: '',
    isReadonly: false,
    commission: '',
    resultsum: ''
  },
  mutations: {
    updateCardNumber(state, cardNumber) {
      state.cardNumber = cardNumber
    },
    updateCardName(state, cardName) {
      state.cardName = cardName
    },
    updateCardMounth(state, mounth) {
      state.cardMounth = mounth
    },
    updateCardYear(state, year) {
      state.cardYear = year
    },
    updateCardCCV(state, cardCCV) {
      state.cardCCV = cardCCV
    },
    updateReadOnly(state, isReadonly) {
      state.isReadonly = isReadonly
    },
    updateCommission(state, commission) {
      state.commission = commission
    },
    updateResultSum(state, resultsum) {
      state.resultsum = resultsum
    },
  },
  getters: {  
    getIsReadOnly(state) {
      return state.isReadonly
    },
    getCardNumber(state) {
      return state.cardNumber
    },
    getCardName(state) {
      return state.cardName
    },
    getCardMounth(state) {
      return state.cardMounth
    },
    getCardYear(state) {
      return state.cardYear
    },
    getCardCCV(state) {
      return state.cardCCV
    },
    getCommission(state) {
      return state.commission
    },
    getResultSum(state) {
      return state.resultsum
    }
  }
})
