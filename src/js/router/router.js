import Vue from 'vue'
import VueRouter from "vue-router";
import Pay from '../components/AppComponents/Pay.vue';
import PayError from '../components/AppComponents/PayError.vue';
import PaySuccess from '../components/AppComponents/PaySuccess.vue';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: Pay
  },
  {
    path: '/PaySuccess',
    component: PaySuccess
  },
  {
    path: '/PayError',
    component: PayError
  }
]

const router = new VueRouter({
  routes,
  mode: "history"
})

export default router
