import Vue from 'vue'
import router from './router/router'
import store from './vuex/store'

import App from './components/App.vue'

Vue.component("App", App)

new Vue({
  el: "#app",
  router,
  store,
})