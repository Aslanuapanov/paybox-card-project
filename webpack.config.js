const path = require('path');
const VueLoaderPlugin = require("vue-loader/lib/plugin");

module.exports = {
    mode: "development",
    entry: ['./src/js/app.js'],
    output: {
        filename: 'main.js',
        path: path.join(__dirname, './dist/js')
    },
    devServer: {
        contentBase: path.join(__dirname, ''),
        compress: true,
        port: 9000,
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: "babel-loader",
            },
            {
                test: /\.vue$/,
                loader: "vue-loader",
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.scss$/,
                use: ["vue-style-loader", "css-loader", "sass-loader"],
            },
        ]
    },
    plugins: [new VueLoaderPlugin()],
    resolve: {
        alias: {
            vue: "vue/dist/vue.js",
        },
    },
};
